const express = require("express");
const router = express.Router();

const users={};
router.post("/signup", async (req, res) => {
  
  if (users.hasOwnProperty(req.body.email)) {
    res.status(400).json({ message: "User already exists" });
  }
  else {
      let query = {
        name: req.body.name,
        password: req.body.password,
        channels:[]
      };
      users[req.body.email]=query;
      res.status(200).json({ doc:users[req.body.email] ,message: "success" });
  }
});


router.post("/login", async (req, res) => {

  if (!users.hasOwnProperty(req.body.email)) {
    res.status(401).json({ message: "Email not recognized" });
    return;
  }

  if (users[req.body.email].password!==req.body.password) {
    res.status(401).json({
      message: "Wrong password"
    });
   
    return;
  }
  res.status(200).json({
    doc:users[req.body.email],
    message: "authorized"
  });
});
router.post("/join-channel", async (req, res) => {

users[req.body.email].channels.push(req.body.channelName);
  res.status(200).json({
    doc:users[req.body.email].channels,
    message: "channel-joined"
  });
});
router.get("/channels/:email", async (req, res) => {
    res.status(200).json({
      doc:users[req.params.email].channels,
      message: "joined channels"
    });
  });




module.exports = router;
