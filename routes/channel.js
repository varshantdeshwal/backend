const express = require("express");
const router = express.Router();

const channels = {};
router.post("/", async (req, res) => {
 
    channels[req.body.name]= { purpose: req.body.purpose, messages: [] }
    
    res.status(200).json({ message: "success" });
});

router.post("/add-message", async (req, res) => {

  channels[req.body.name].messages.unshift({text:req.body.message, sender:req.body.sender});
  res.status(200).json({
    message: 'success'
  });
});

router.get("/", async (req, res) => {
        res.status(200).json({ doc: Object.keys(channels), message: "channel list" });
});
router.get("/get-messages/:name", async (req, res) => {
  console.log(req.params);
  res.status(200).json({ doc: channels[req.params.name].messages, message: "channel messages" });
});
module.exports = router;
