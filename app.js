const express = require("express");
const app = express();

var socket = require('socket.io');
const {user:userRoutes, channel:channelRoutes} = require("./routes");
const bodyParser = require("body-parser");

app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

app.use("/user", userRoutes);
app.use('/channel', channelRoutes);
const port = process.env.PORT || 3048;

var server=app.listen(port, () => console.log(`listening at ${port}`));

io = socket(server);

io.on('connection', (socket) => {
  console.log(socket.id);

  socket.on('SEND_MESSAGE', function(data){
      io.emit('RECEIVE_MESSAGE', data);
  });
  socket.on('ADD_CHANNEL', function(data){
    io.emit('GET_CHANNEL', data);
});
});